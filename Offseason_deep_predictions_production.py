# -*- coding: utf-8 -*-
import pyodbc
from keras.layers import Input, Dense, normalization,Dropout
from keras.layers.merge import Add
from keras.models import Model
from sklearn.preprocessing import StandardScaler
#from keras import regularizers
from keras.callbacks import EarlyStopping
import pandas as pd
import numpy as np
import datetime
"""
Created on Sun Aug 20 15:00:36 2017
Script to generate and populate baseline game by gamenumbers with deep NNs in best
manner I've found thus far.

One note--I haven't tested regularization with residual nets, besides dropout...
e.g. haven't enforced any weight decay, which I like from a theoretical POV...
@author: krush
"""

def build_data():
    """Returns unscaled game-by-game ticketing data we have found to be relevant.
    Depends on creation of view RegressorsView in BrewForecast, which I have not
    yet done as of August 20 2017.
    """
    cnxn=pyodbc.connect('DRIVER={SQL Server};SERVER=BREWBI-DB;DATABASE=BrewForecast;UID=PythonExecutor;PWD=2Cons!stant')
    cursor=cnxn.cursor()
    cols=list()
    base_data=pd.DataFrame()
    for row in cursor.columns(table='GameAttributes'):
        cols.append(row.column_name)
    try:
        cursor.execute("Select * from GameAttributes")
        for ix, row in enumerate(cursor.fetchall()):
            base_data=base_data.append(pd.DataFrame(pd.DataFrame(np.array(row)).transpose()))
        base_data=base_data.rename(columns=pd.Series(cols))
        base_data['Date']=pd.to_datetime(base_data['Date'], format='%Y-%m-%d').dt.date
        total_sold=pd.DataFrame()        
        cursor.execute("""select GameId, 
        PackageTickets, 
        GroupTickets, 
        INDDiscTickets, 
        INDFullTickets from BOByGame""" )
        for row in cursor.fetchall():
            total_sold=total_sold.append(pd.DataFrame(pd.DataFrame(np.array(row)).transpose()))
        total_sold=total_sold.rename(columns=pd.Series(['GameId',  'PackageTickets', 'GroupTickets', 'INDDiscTickets', 'INDFullTickets']))
        base_data=base_data.merge(total_sold, how='inner', on='GameId')
        to_delete=['MIL Opening Day Prospect Rankings', 'Mil Previous Season NLDS?', 
            'Opponent - Previous Season - GB Division', 'Opponent - Previous Season - GB Wild Card',
            'Opponent - Previous Season Playoffs?', 'Opponent - Previous Season Wild Card?',
            'Opponent - Previous Season NL/ALDS']
        for string in to_delete:
            del base_data[string]
        #observed=base_data[base_data['Date']<datetime.date.today()]
        #unobserved=base_data[base_data['Date']>=datetime.date.today()]
        observed=base_data[base_data['Year']<2017]
        unobserved=base_data[base_data['Year']>=2017]
    finally:
        cnxn.close()
    return {'observed':observed, 'unobserved':unobserved}
    
def write_baseline_predictions():
    """Writes our baseline predictions to BrewForecast
    """
    return True

def functional_model(*,input_dim, width, depth, skip_length, prob):
    """Builds deep NN model using Keras functional API.
    Which is a beautiful interface"""
    
    starting_point=0
    inputs=Input(shape=(input_dim,))
    x=normalization.BatchNormalization()(inputs)
    x=Dense(width, activation='relu')(inputs)
    x=normalization.BatchNormalization()(x)
    for k in range(depth):
        if k%skip_length==0:
            if starting_point!=0:
                x=Add()([x, starting_point])
            starting_point=x
        x=Dense(width, activation='relu')(x)
        x=Dropout(prob)(x)
        x=normalization.BatchNormalization()(x)
    prediction=Dense(1, activation='linear')(x)
    model=Model(inputs=inputs, outputs=prediction)
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model
    
def modeler_and_predictor(*,name, train_data, to_predict, num_iter=15, num_to_throw_out=5):
    """Hardcoded parameters for these models, built essentially with cross-
    validation
    """
    train_x, train_y=train_data
    To_predict_IDs=pd.DataFrame(to_predict['GameId'], columns=['GameId'])
    To_predict_IDs.reset_index(drop=True, inplace=True)
    to_delete=['GameId', 'Date']
    for string in to_delete:
        del train_x[string]
        del to_predict[string]
    scaler=StandardScaler().fit(train_x)
    train_x=pd.DataFrame(scaler.transform(train_x), columns=list(train_x))
    to_predict=pd.DataFrame(scaler.transform(to_predict), columns=list(to_predict))
    w=120
    d=12
    p=.18
    skips=3
    pat=np.random.randint(low=100, high=120)
    early_stopping=EarlyStopping(monitor='val_loss', min_delta=1, patience=pat)
    model=functional_model(input_dim=len(train_x.columns),width=w, depth=d, skip_length=skips,prob=p)
    running_predictions=pd.DataFrame([0]*len(to_predict))
    for k in range(num_iter):
        msk=np.random.rand(len(train_x))<0.8
        val_x=train_x[~msk]
        val_y=train_y[~msk]
        split_x=train_x[msk]
        split_y=train_y[msk]
        model.fit(np.asmatrix(split_x), np.asmatrix(split_y).transpose(),
                  validation_data=(np.asmatrix(val_x), np.asmatrix(val_y).transpose()),
                    callbacks=[early_stopping], epochs=5000, batch_size=100, verbose=0)
        if k>=num_to_throw_out:
            running_predictions=running_predictions+pd.DataFrame(model.predict(np.asmatrix(to_predict)))
    print(running_predictions/(k-num_to_throw_out))
    To_predict_IDs[train_y.name]=running_predictions/(k-num_to_throw_out+1)
    print(To_predict_IDs)
    return To_predict_IDs

def main():
    built_data=build_data()
    observed_data=built_data['observed']
    to_predict_data=built_data['unobserved']
    response_dict={}
    Predictions=pd.DataFrame({'GameId':to_predict_data['GameId'], 'Date':to_predict_data['Date']})
    for string in ['PackageTickets', 'GroupTickets', 'INDFullTickets', 
    'INDDiscTickets']:
        response_dict[string]=observed_data[string]
        del observed_data[string]
        del to_predict_data[string]
    response_dict['AllND']=response_dict['PackageTickets']+response_dict['GroupTickets']\
+response_dict['INDFullTickets']+response_dict['INDDiscTickets']
    for string in ['AllND','PackageTickets', 'GroupTickets', 'INDFullTickets', 
    'INDDiscTickets']:
        preds=modeler_and_predictor(name=string,
                    train_data=(observed_data.copy(), response_dict[string].copy()),
                                to_predict=to_predict_data.copy(), num_iter=20,
                                num_to_throw_out=10)
        Predictions=pd.merge(Predictions, pd.DataFrame(preds), 
                             how='inner',on='GameId')
    #write_baseline_predictions(Predictions)
    return Predictions
    
    
    
if __name__ =="__main__":
    Predictions=main()