# -*- coding: utf-8 -*-
from sklearn.grid_search import GridSearchCV
from sklearn import ensemble
"""
Created on Wed Jan 18 11:29:38 2017
Offseason aggregate ticket estimator

For in-season use as a prior



@author: krush
"""
def create_package_data(data1):

    del data1['HOU']
    del data1['Flash Sale']
#    del data1['Miller Mondays']
#    del data1['Student Night']
#    del data1['Opponent - Previous Season NL/ALDS']
#    del data1['Opponent - Previous Season NL/ALCS']
#    del data1['Major League Night']
#    del data1['Other Theme Nights']
    del data1['Super Bonus']
    del data1['Opponent - Previous Season - GB Wild Card']
    del data1['Opponent - Previous Season Wild Card?']
    del data1['Opponent - Previous Season - GB Division']
    del data1['Opponent - Previous Season NL/ALDS']
    del data1['Opponent - Previous Season NL/ALCS']
    del data1['Opponent - Previous Season World Series Champ']
    return data1
#
#    
def create_group_data(data1):
#    del data1['Opponent - Previous Season NL/ALDS']
#    del data1['Opponent - Previous Season NL/ALCS']
#    del data1['WSN']
#    del data1['COL']
#    del data1['SDP']
#    del data1['LAA']
#    del data1['Major League Night']
#    del data1['Star Wars Day']
#    del data1['Other Theme Nights']
#    del data1['Super Bonus']
    return data1
#
#    
def create_ind_full_data(data1):
    del data1['Super Bonus']
    del data1['Mon']
    del data1['LAD']
    del data1['Previous Season TV Ratings']
    del data1['Opponent - Previous Season - GB Wild Card']
    del data1['Opponent - Previous Season Wild Card?']
    del data1['Opponent - Previous Season - GB Division']
    del data1['Opponent - Previous Season NL/ALDS']
    del data1['Opponent - Previous Season NL/ALCS']
    del data1['Opponent - Previous Season World Series Champ']
    return data1
#    
def create_ind_disc_data(data1):
    del data1['Super Bonus']
    #del data1['MIL Opening Day Prospect Rankings']
    del data1['LAD']
    del data1['Opponent - Previous Season - GB Wild Card']
    del data1['Opponent - Previous Season Wild Card?']
    del data1['Opponent - Previous Season - GB Division']
    del data1['Opponent - Previous Season NL/ALDS']
    del data1['Opponent - Previous Season NL/ALCS']
    del data1['Opponent - Previous Season World Series Champ']
    return data1

def model_builder(train_data_x, train_data_y, test_data_x, test_data_y):
    #import pandas as pd
    #import numpy as np
    from sklearn import linear_model
    from sklearn import ensemble  
    from sklearn.preprocessing import PolynomialFeatures
    #from sklearn.preprocessing import StandardScaler
    from sklearn.pipeline import Pipeline
    from sklearn.kernel_ridge import KernelRidge
    #from sklearn import svm
    from sklearn.grid_search import GridSearchCV
                       
    
    print("Starting with the plainer models:")
    
    
    #Vanilla degree-2 polynomial regression
    degree_2_linear_model=Pipeline([('poly', PolynomialFeatures(degree=2)),
                                    ('linear', linear_model.LinearRegression())])
    degree_2_lin_regression=degree_2_linear_model.fit(train_data_x,
                                                      train_data_y)
    #second_degree_vanilla_fit=degree_2_lin_regression.score(train_data_x,
                                                            #train_data_y)
    #print("Vanilla quadratic R^2 from fitted data is "+str(second_degree_vanilla_fit))
    vanilla_quadratic_score=degree_2_lin_regression.score(test_data_x, 
                                                          test_data_y)
    print("Vanilla quadratic R^2, test data is "+str(vanilla_quadratic_score))
    #print("Vanilla quad 2016 prediction is "+str(degree_2_linear_model.predict(X2016_data).sum()))
    
    #Bagged quadratic regression
    bagged_quad_vanilla=ensemble.BaggingRegressor(base_estimator=degree_2_linear_model,
                                                  max_samples=100, oob_score=False)
    bagged_quad_vanilla.fit(train_data_x, train_data_y)
    #bagged_quad_vanilla_fit=bagged_quad_vanilla.score(train_data_x, train_data_y)
    #print("Bagged quadratic R^2 from fitted data is "+str(bagged_quad_vanilla_fit))
    bagged_quad_vanilla_score=bagged_quad_vanilla.score(test_data_x, test_data_y)
    print("Bagged quadratic R^2, test data is "+str(bagged_quad_vanilla_score))
    #print("Bagged quad 2016 prediction is "+str(bagged_quad_vanilla.predict(X2016_data).sum()))
    
    #Cross-validated ridge regression
    ridge_reg=linear_model.RidgeCV(alphas=[0.001, 0.01, 0.1, 1.0, 10.0])
    ridge_reg.fit(train_data_x, train_data_y)
    #ridge_reg_fit=ridge_reg.score(train_data_x, 
                                              #train_data_y)
    #print("Ridge regression R^2 from fitted data is "+str(ridge_reg_fit))
    ridge_reg_score=ridge_reg.score(test_data_x, 
                                              test_data_y)
    print("Ridge regression R^2, test data is "+str(ridge_reg_score))
    #print("Ridge 2016 prediction is "+str(ridge_reg.predict(X2016_data).sum()))
    
    #Cross-validated lasso_model
    lasso=linear_model.LassoLarsIC(criterion='bic')
    lasso.fit(train_data_x, train_data_y)
    #lasso_model_fit=lasso.score(train_data_x,train_data_y)
    #print("Lasso linear R^2 from fitted data is "+str(lasso_model_fit))
    lasso_score=lasso.score(test_data_x, test_data_y)
    print("Lasso linear R^2, test data is "+str(lasso_score))
    #print("Lasso linear 2016 prediction is "+str(lasso.predict(X2016_data).sum()))
    
    #Quadratic lasso regression
    quad_lasso=Pipeline([('poly', PolynomialFeatures(degree=2)),
                                    ('linear', linear_model.LassoLarsIC(criterion='bic'))])
    quad_lasso.fit(train_data_x,train_data_y)
    #second_degree_lasso_fit=quad_lasso.score(train_data_x,
                                                              #train_data_y)
    #print("Lasso quadratic R^2 from fitted data is "+str(second_degree_lasso_fit))
    quad_lasso_score=quad_lasso.score(test_data_x, 
                                              test_data_y)
    print("Lasso quadratic R^2, test data is "+str(quad_lasso_score))
    #print("Lasso quad 2016 prediction is "+str(quad_lasso.predict(X2016_data).sum()))
    
    
    #Cross-validated orthogonal matching pursuit
    omp=linear_model.OrthogonalMatchingPursuitCV()
    omp.fit(train_data_x, train_data_y)
    #omp_fit=omp.score(train_data_x, train_data_y)
    #print("Orthogonal matching pursuit R^2 from fitted data is "
          #+str(omp_fit))
    omp_score=omp.score(test_data_x, test_data_y)
    print("Orthogonal matching pursuit R^2, test data is "+str(omp_score))
    #print("OMP 2016 prediction is "+str(omp.predict(X2016_data).sum()))
    
    #Baysian ridge regression
    br=linear_model.BayesianRidge()
    br.fit(train_data_x, train_data_y)
    #bayesian_ridge_fit=br.score(train_data_x, 
                                                    #train_data_y)
    #print("Bayesian ridge R^2 from fitted data is "
          #+str(bayesian_ridge_fit))
    bayesian_ridge_score=br.score(test_data_x, 
                                              test_data_y)
    print("Bayesian ridge R^2, test data is "+str(bayesian_ridge_score))
    #print("Bayesian ridge 2016 prediction is "+str(br.predict(X2016_data).sum()))
    
    ARD=linear_model.ARDRegression()
    ARD.fit(train_data_x, train_data_y)
    #ARD_fit=ARD.score(train_data_x, train_data_y)
    ARD_score=ARD.score(test_data_x, test_data_y)
    #print("ARD fitted R^2 is "+str(ARD_fit))
    print("ARD R^2, test_data is "+str(ARD_score))
    #print("ARD 2016 prediction is "+str(ARD.predict(X2016_data).sum()))
    
    print("Crazier models upcoming:")
    #Cross-validated Kernel ridge regression
    kr_parameters={'kernel':('linear', 'poly', 'rbf'), 'alpha':(0.001, 0.01,
                   0.1, 1), 'degree':(1, 2, 3, 4)}
    kr_clf=GridSearchCV(KernelRidge(), kr_parameters, cv=10)
    kr_clf.fit(train_data_x, train_data_y)
    kr=kr_clf.best_estimator_
    #kr_fit=kr.score(train_data_x, train_data_y)
    #print("Kernel ridge CV R^2 from fitted data is "
          #+str(kr_fit))
    kr_score=kr.score(test_data_x, 
                                              test_data_y)
    print("Kernel ridge CV R^2, test data is "+str(kr_score))
    #print("KR 2016 prediction is "+str(kr.predict(X2016_data).sum()))
                     
    #Random forest regressor
    rfr_parameters={'n_estimators':(10, 20, 50, 100), 'max_depth':(3, 5, 10)}
    rfr_clf=GridSearchCV(ensemble.RandomForestRegressor(), rfr_parameters, cv=10)
    rfr_clf.fit(train_data_x, train_data_y)
    rfr=rfr_clf.best_estimator_
    #rfr_fit=rfr.score(train_data_x, train_data_y)
    rfr_score=rfr.score(test_data_x, test_data_y)
    #print("Random forest regressor R^2 from fitted data is "+str(rfr_fit))
    print("Random forest refressor R^2, test data is "+str(rfr_score))
    #print("RF 2016 prediction is "+str(rfr.predict(X2016_data).sum()))
    
    #Adaboost
    ab_parameters={'n_estimators':(10, 20, 50, 100)}
    ab_clf=GridSearchCV(ensemble.AdaBoostRegressor(), ab_parameters, cv=10)
    ab_clf.fit(train_data_x, train_data_y)
    ab=ab_clf.best_estimator_
    #ab_fit=ab.score(train_data_x, train_data_y)
    ab_score=ab.score(test_data_x, test_data_y)
    #print("Adaboost regressor R^2 from fitted data is "+str(ab_fit))
    print("Adaboost regressor R^2, test data is "+str(ab_score))
    #print("Adaboost 2016 prediction is "+str(ab.predict(X2016_data).sum()))
    
    #Gradient boosting regressor
    gb_params={'n_estimators':(50, 100, 200),'max_depth':(3, 5)}
    gb_clf=GridSearchCV(ensemble.GradientBoostingRegressor(loss='huber'), gb_params, cv=10)
    gb_clf.fit(train_data_x, train_data_y)
    gb=gb_clf.best_estimator_
    #gb_fit=gb.score(train_data_x, train_data_y)
    gb_score=gb.score(test_data_x, test_data_y)
    #print("GBRF regressor R^2 from fitted data is "+str(gb_fit))
    print("GBRF regressor R^2, test data is "+str(gb_score))
    #print("GBRF 2016 prediction is "+str(gb.predict(X2016_data).sum()))



def fake_season(i):
         #Fake season is made to score errors of modeling technique. Draw
        #the data from a set disjoint from the training set
    new_data=pd.DataFrame(columns=col_names)
    for k in range(i):
        #First number controls where the training data is drawn from; year cutoffs
        #are 81, 162, 243, 324, 405
        subset_choice=list(np.random.choice(range(324, 405),size=(1, 81), replace=False))
        sim_season=pd.DataFrame(columns=col_names)
        for j in range(1):
            sim_season=sim_season.append(data.iloc[subset_choice[j]])
        aggregated_season=sim_season.sum(axis=0)
        ag_season_df=aggregated_season.to_frame()
        ag_season_df=ag_season_df.transpose()
        new_data=new_data.append(ag_season_df)
    return new_data


def score_ag_fake_season(new_data, package_2017, package_2016,
                                  group_2017, group_2016,
                                  ind_disc_2017, ind_disc_2016,
                                  ind_full_2017, ind_full_2016):
        #Splitting off the y variables from the training data
        
#    from sklearn.kernel_ridge import KernelRidge
    #from sklearn import svm
    train_package=new_data['Package']
    train_group=new_data['True Groups']
    train_ind_full=new_data['IND Full Price']
    train_ind_disc=new_data['IND Discounts + Free Tickets']
    for string in {'Package', 'True Groups', 'IND Full Price', 'IND Discounts + Free Tickets'}:
        del new_data[string]

    to_score=fake_season(1)
    test_package=to_score['Package']
    test_group=to_score['True Groups']
    test_ind_full=to_score['IND Full Price']
    test_ind_disc=to_score['IND Discounts + Free Tickets']
    for string in {'Package', 'True Groups', 'IND Full Price', 'IND Discounts + Free Tickets'}:
        del to_score[string]
    
    package_train_data=create_package_data(new_data.copy())
    group_train_data=create_group_data(new_data.copy())
    ind_full_train_data=create_ind_full_data(new_data.copy())
    ind_disc_train_data=create_ind_disc_data(new_data.copy())
    
    package_test_data=create_package_data(to_score.copy())
    group_test_data=create_group_data(to_score.copy())
    ind_full_test_data=create_ind_full_data(to_score.copy())
    ind_disc_test_data=create_ind_disc_data(to_score.copy())
    
    #StandardScaler to center and rescale the aggregated data, toggled on/off as desired.
    package_scaler=StandardScaler().fit(pd.concat([package_train_data, package_test_data]))
    group_scaler=StandardScaler().fit(pd.concat([group_train_data, group_test_data]))
    ind_disc_scaler=StandardScaler().fit(pd.concat([ind_disc_train_data, ind_disc_test_data]))
    ind_full_scaler=StandardScaler().fit(pd.concat([ind_full_train_data, ind_full_test_data]))
    #Applying the correct scalers
    package_2016=package_scaler.transform(package_2016)
    package_2017=package_scaler.transform(package_2017)
    package_test=package_scaler.transform(package_test_data)
    package_train_data=package_scaler.transform(package_train_data)
    group_2016=group_scaler.transform(group_2016)
    group_2017=group_scaler.transform(group_2017)
    group_test=group_scaler.transform(group_test_data)
    group_train_data=group_scaler.transform(group_train_data)
    ind_disc_2016=ind_disc_scaler.transform(ind_disc_2016)
    ind_disc_2017=ind_disc_scaler.transform(ind_disc_2017)
    ind_disc_test=ind_disc_scaler.transform(ind_disc_test_data)
    ind_disc_train_data=ind_disc_scaler.transform(ind_disc_train_data)
    ind_full_2016=ind_full_scaler.transform(ind_full_2016)
    ind_full_2017=ind_full_scaler.transform(ind_full_2017)
    ind_full_test=ind_full_scaler.transform(ind_full_test_data)
    ind_full_train_data=ind_full_scaler.transform(ind_full_train_data)
    
    #Fitting the models to the training data
    #ARD=linear_model.ARDRegression()
    pk_lasso=linear_model.LassoLarsIC(criterion='bic')
    pk_lasso.fit(package_train_data, train_package)
    pk_test_pred=pk_lasso.predict(package_test)
    #ARD.fit(package_train_data, train_package)
    pk_test_pred=pk_lasso.predict(package_test)
#    pk_2016_pred=pk_lasso.predict(package_2016)
    pk_2017_pred=pk_lasso.predict(package_2017)

    ab_parameters={'n_estimators':(10, 20, 50, 100)}
    ab_clf=GridSearchCV(ensemble.AdaBoostRegressor(), ab_parameters, cv=10)
    ab_clf.fit(group_train_data, train_group)
    ab=ab_clf.best_estimator_
    gp_test_pred=ab.predict(group_test)
#    gp_2016_pred=ab.predict(group_2016)
    gp_2017_pred=ab.predict(group_2017)
    
    ID_lasso=linear_model.LassoLarsIC(criterion='bic')
    ID_lasso.fit(ind_disc_train_data, train_ind_disc)
    ID_test_pred=ID_lasso.predict(ind_disc_test)
#    ID_2016_pred=ID_lasso.predict(ind_disc_2016)
    ID_2017_pred=ID_lasso.predict(ind_disc_2017)
    
#    kr_parameters={'kernel':('linear', 'poly', 'rbf'), 'alpha':(0.001, 0.01,
#                   0.1, 1), 'degree':(1, 2, 3, 4)}
#    kr_clf=GridSearchCV(KernelRidge(), kr_parameters, cv=10)
#    kr_clf.fit(ind_full_train_data, train_ind_full)
#    IF_KR=kr_clf.best_estimator_
#    IF_KR.fit(ind_full_train_data, train_ind_full)
#    
#    IF_ridge=linear_model.RidgeCV(alphas=[0.001, 0.01, 0.1, 1.0, 10.0])
#    IF_ridge.fit(ind_full_train_data, train_ind_full)

    IF_lasso=linear_model.LassoLarsIC(criterion='bic')
    IF_lasso.fit(ind_full_train_data, train_ind_full)
    IF_test_pred=IF_lasso.predict(ind_full_test)
#    IF_2016_pred=IF_lasso.predict(ind_full_2016)
    IF_2017_pred=IF_lasso.predict(ind_full_2017)

    
    tot_test_pred=pk_test_pred.sum()+gp_test_pred.sum()+ID_test_pred.sum()+IF_test_pred.sum()
    #tot_2016=pk_2016_pred.sum()+gp_2016_pred.sum()+ID_2016_pred.sum()+IF_2016_pred.sum()
    tot_2017=pk_2017_pred.sum()+gp_2017_pred.sum()+ID_2017_pred.sum()+IF_2017_pred.sum()
    tot_true=test_package.sum()+test_group.sum()+test_ind_disc.sum()+test_ind_full.sum()
    #Just informative print statements
    
#    print("Package error is "+str(pk_test_pred-test_package.sum()))
#    print("Groups error is "+str(gp_test_pred-test_group.sum()))
#    print("Indy disc is "+str(ID_test_pred-test_ind_disc.sum()))   
#    print("Indy full error is "+str(IF_test_pred-test_ind_full.sum()))
#    
#    print("2016 package prediction is "+str(pk_2016_pred))
#    print("2016 group prediction is "+str(gp_2016_pred))
#    print("2016 IND discount prediction is "+str(ID_2016_pred))   
#    print("2016 IND full lasso prediction is "+str(IF_2016_pred))
    
    print("2017 package prediction is "+str(pk_2017_pred))
    print("2017 group prediction is "+str(gp_2017_pred))
    print("2017 IND discount prediction is "+str(ID_2017_pred))   
    print("2017 IND full prediction is "+str(IF_2017_pred))
    
    

    
    print("Total error is "+str(tot_test_pred-tot_true))
    print("2017 true prediction is "+str(tot_2017))
    
    pred_vector=pd.DataFrame({'Package prediction': [pk_2017_pred.sum()],
                                    'Groups prediction': [gp_2017_pred.sum()],
                                    'IND full prediction': [IF_2017_pred.sum()],
                                    'IND discount prediction':[ID_2017_pred.sum()],
                                    'Total 2017 prediction':[tot_2017]})
    

    return pred_vector

    
    
    
import numpy as np
import pandas as pd
from sklearn import linear_model  
import warnings
warnings.filterwarnings("ignore")
import time
start=time.time()
from sklearn.preprocessing import StandardScaler


data=pd.read_csv("C:/Users/krush/Documents/2017 FSE estimate supplementary docs/2017 FSE regression docs/Keith 2017 FSE estimate/Final_modeling_data/2_6_17_data/Thru_2016_data1.csv")
#The duplication of the payroll seems to have been causing a ton of instability. This is something to watch out for!
del data['MIL Current Season Payroll (Millions) ']
del data['MIL Opening Day Prospect Rankings']
del data['BOS']
del data['Opponent Current Season Payroll (Millions)']
aX2016_data=data[data['Year']==2016]
del data['Year']
del aX2016_data['Year']
del aX2016_data['Package']
del aX2016_data['IND Discounts + Free Tickets']
del aX2016_data['IND Full Price']
del aX2016_data['True Groups']

col_names=list(data)
#CURRENTLY READING IN 2016 Performance data
X2017_data=pd.read_excel("C:/Users/krush/Documents/2017 FSE estimate supplementary docs/2017 FSE regression docs/Keith 2017 FSE estimate/Final_modeling_data/X2017_data_other_seasons.xlsx")
del X2017_data['Package']
del X2017_data['IND Discounts + Free Tickets']
del X2017_data['IND Full Price']
del X2017_data['True Groups']
del X2017_data['MIL Current Season Payroll (Millions) ']
del X2017_data['BOS']
del X2017_data['Opponent Current Season Payroll (Millions)']
del X2017_data['MIL Opening Day Prospect Rankings']

X2016_data=aX2016_data.sum(axis=0).to_frame().transpose()
ag_2017=X2017_data.sum(axis=0).to_frame().transpose()

package_2017=create_package_data(ag_2017.copy())
package_2016=create_package_data(X2016_data.copy())
group_2017=create_group_data(ag_2017.copy())
group_2016=create_group_data(X2016_data.copy())
ind_disc_2017=create_ind_disc_data(ag_2017.copy())
ind_disc_2016=create_ind_disc_data(X2016_data.copy())
ind_full_2017=create_ind_full_data(ag_2017.copy())
ind_full_2016=create_ind_full_data(X2016_data.copy())
package_cols=list(package_2017)
gp_cols=list(group_2017)
ID_cols=list(ind_disc_2017)
IF_cols=list(ind_full_2017)


cols=['Package error', 'Groups error', 'IND full error', 'IND discount error', 'Total error', 'Total 2016 prediction']
errors=pd.DataFrame(columns=cols)
package_coeff=pd.DataFrame(columns=package_cols)
gp_coeff=pd.DataFrame(columns=gp_cols)
#IF_coeff=pd.DataFrame(columns=IF_cols)
ID_coeff=pd.DataFrame(columns=ID_cols)
package_deltas=pd.DataFrame(columns=package_cols)
gp_deltas=pd.DataFrame(columns=gp_cols)
IF_deltas=pd.DataFrame(columns=IF_cols)
ID_deltas=pd.DataFrame(columns=ID_cols)


cols=['Package prediction', 'Groups prediction', 'IND full prediction', 'IND discount prediction', 'Total 2017 prediction']
predictions=pd.DataFrame(columns=cols)
for j in range(2000):
    new_data=pd.DataFrame(columns=col_names)
    for k in range(2000):
        #First number controls where the training data is drawn from; year cutoffs
        #are 81, 162, 243, 324, 405
        subset_choice=list(np.random.choice(405,size=(1, 81), replace=False))
        sim_season=pd.DataFrame(columns=col_names)
        for j in range(1):
            sim_season=sim_season.append(data.iloc[subset_choice[j]])
        aggregated_season=sim_season.sum(axis=0)
        ag_season_df=aggregated_season.to_frame()
        ag_season_df=ag_season_df.transpose()
        new_data=new_data.append(ag_season_df)
    
#This is for error estimate and/or 2017 predictions

    returned=score_ag_fake_season(new_data, package_2017, package_2016,
                                  group_2017, group_2016,
                                  ind_disc_2017, ind_disc_2016,
                                  ind_full_2017, ind_full_2016)
    predictions=predictions.append(returned)


#The following is for pure model selection

#    test=fake_season(1000).copy()
#    train=new_data.copy()
#
#    train_package=train['Package']
#    train_group=train['True Groups']
#    train_ind_full=train['IND Full Price']
#    train_ind_disc=train['IND Discounts + Free Tickets']
#    for string in {'Package', 'True Groups', 'IND Full Price', 'IND Discounts + Free Tickets'}:
#        del train[string]
#
#    test_package=test['Package']
#    test_group=test['True Groups']
#    test_ind_full=test['IND Full Price']
#    test_ind_disc=test['IND Discounts + Free Tickets']
#    for string in {'Package', 'True Groups', 'IND Full Price', 'IND Discounts + Free Tickets'}:
#        del test[string]
#    
#    a=StandardScaler()
#    a.fit(pd.concat([train, test]))
#    train=a.transform(train)
#    X2016_data=a.transform(X2016_data)
#    X2017_data=a.transform(X2017_data)
#    test=a.transform(test)
#
#    train_data_x=train
#    test_data_x=test
#    for [string, train_y, test_y] in [['Package', train_package, test_package], ['Group',train_group, test_group], 
#         ['IND Full',train_ind_full, test_ind_full], ['IND disc',train_ind_disc, test_ind_disc]]:
#        train_data_y=train_y
#        test_data_y=test_y
#        print(string+" models follow:")
#        model_builder(train_data_x, train_data_y, test_data_x, test_data_y)
#    X2016_data=a.inverse_transform(X2016_data)
#    X2017_data=a.inverse_transform(X2017_data)

print("Elapsed time: "+str(time.time()-start))

