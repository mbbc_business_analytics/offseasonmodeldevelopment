# -*- coding: utf-8 -*-
import pyodbc
from keras.layers import Input, Dense, normalization,Dropout
from keras.layers.merge import Add
from keras.models import Model
from sklearn.preprocessing import StandardScaler
#from keras import regularizers
from keras.callbacks import EarlyStopping
import pandas as pd
import numpy as np
import datetime
from sklearn import linear_model
from sklearn.grid_search import GridSearchCV
from sklearn import ensemble
#import pdb
import warnings
warnings.filterwarnings("ignore")
ab_parameters={'n_estimators':(10, 20, 50, 100)}
"""
Created on Mon Aug 28 14:16:33 2017
First script to assign ticket values to 2018.
First implementation will be pure game-by-game, functions will be added quickly
to simulate season overall, as well as introduce uncertainty into team performance scenarios.

@author: krush
"""
class Error(Exception):
    """Base class for other exceptions"""
    pass
class SenselessQuestionError(Error):
    """Raised when certain variable combinations make no business sense"""
    pass


class SeasonTickets(object):
    """
    Contains season-level ticketing attributes as well as a collection of Game
    objects. A few Boolean variables determine exactly what variables we are
    allowed to look at.
    Also the Year...
    """
    def __init__(self, Year=2018, pack_prediction=0, group_prediction=0,
                 inddisc_prediction=0, indfull_prediction=0, Games=[],
                is_preseason_estimate=True, sales_info_allowed=False,
                package_model_built=False, group_model_built=False,
                indfull_model_built=False, inddisc_model_built=False,
                package_model=None, group_model=None,
                indfull_model=None, inddisc_model=None):
        self.Year=Year
        self.pack_prediction=pack_prediction
        self.group_prediction=group_prediction
        self.indfull_prediction=inddisc_prediction
        self.indfull_prediction=indfull_prediction
        self.Games=Games
        ##The following two are in fact distinct; removing all bias we can
        ## by essentially examining leave-one-out residuals is reasonable,
        ## as is baselining by examining entirely preseason estimates.
        self.is_preseason_estimate=is_preseason_estimate
        self.sales_info_allowed=sales_info_allowed
        self.package_model_built=package_model_built
        self.group_model_built=group_model_built
        self.indfull_model_built=indfull_model_built
        self.inddisc_model_built=inddisc_model_built
        self.package_model=package_model
        self.group_model=group_model
        self.indfull_model=indfull_model
        self.inddisc_model=inddisc_model
        
    @classmethod
    def _query_all_data_preseason(*, year):
        return 0, 1
        
    @classmethod
    def _query_no_sales_data_preseason(*, year):
        return 0, 1
        
    @classmethod
    def _query_all_data(*, year):
        return 0, 1
        
    @classmethod
    def _query_inseason_no_sales_data(*, year):
        return 0, 1
        
    @classmethod
    def _get_package_tickets(*, year):
        return 0
        
    @classmethod
    def _get_group_tickets(*, year):
        return 0
        
    @classmethod
    def _get_inddisc_tickets(*, year):
        return 0
        
    @classmethod
    def _get_indfull_tickets(*, year):
        return 0
        
    @classmethod
    def _build_package_model(self, *, train_data, train_y):
        ##Check for empty
        if train_data.empty:
            return
        ##Fit model
        model=SeasonTickets.Full_season_modeler(name="PackageTickets",
                                               train_data=train_data,
                                               num_seasons=10000)
        ##Return model
        self.package_model=model
        self.package_model_built=True
        return
    
    @classmethod
    def _build_group_model(self, *, train_data, train_y):
        ##Check for empty
        if train_data.empty:
            return
        ##Fit model
        model=SeasonTickets.Full_season_modeler(name="GroupTickets",
                                               train_data=train_data,
                                               num_seasons=10000)
        ##Return model
        self.group_model=model
        self.group_model_built=True
        return 
        
    @classmethod
    def _build_indfull_model(self, *, train_data, train_y):
        ##Check for empty
        if train_data.empty:
            return
        ##Fit model
        model=SeasonTickets.Full_season_modeler(name="INDFullTickets",
                                               train_data=train_data,
                                               num_seasons=10000)
        ##Return model
        self.indfull_model=model
        self.indfull_model_built=True
        return 
        
    @classmethod
    def _build_inddisc_model(self, *, train_data, train_y):
        ##Check for empty
        if train_data.empty:
            return
        ##Fit model
        model=SeasonTickets.Full_season_modeler(name="INDDiscTickets",
                                               train_data=train_data,
                                               num_seasons=10000)
        ##Return model
        self.inddisc_model=model
        self.inddisc_model_built=True
        
    @classmethod
    def Full_season_modeler( *,name, train_data, num_seasons=500):
        """
        Function to build probability distributions for seasonlong numbers, based on
        sampling observed games. Again basically built with CV
        """
        train_x, train_y=train_data
        del train_x['Date']
        fake_x, fake_y=fake_season(num_seasons, train_x.copy(), train_y.copy())

        scaler=StandardScaler().fit(fake_x)
        scaled_x=pd.DataFrame(scaler.transform(fake_x), columns=list(fake_x))
        if name=='PackageTickets':
            model=linear_model.LassoLarsIC(criterion='bic')
            model.fit(scaled_x, fake_y)
        elif name=='GroupTickets':
            ab_clf=GridSearchCV(ensemble.AdaBoostRegressor(), ab_parameters, cv=10)
            ab_clf.fit(scaled_x, fake_y)
        elif name=='INDFullTickets':
            IF_lasso=linear_model.LassoLarsIC(criterion='bic')
            IF_lasso.fit(scaled_x, fake_y)
        elif name=='INDDiscTickets':
            ID_lasso=linear_model.LassoLarsIC(criterion='bic')
            ID_lasso.fit(scaled_x, fake_y)
        elif name=='AllND':
            w=200
            d=20
            p=.3
            skips=3
            pat=np.random.randint(low=100, high=120)
            early_stopping=EarlyStopping(monitor='val_loss', min_delta=1, patience=pat)
            model=functional_model(input_dim=len(train_x.columns),width=w, depth=d, skip_length=skips,prob=p)
            msk=np.random.rand(len(scaled_x))<0.8
            val_x=scaled_x[~msk]
            val_y=fake_y[~msk]
            split_x=scaled_x[msk]
            split_y=fake_y[msk]
            model.fit(np.asmatrix(split_x), np.asmatrix(split_y).transpose(),
                      validation_data=(np.asmatrix(val_x), np.asmatrix(val_y).transpose()),
                        callbacks=[early_stopping], epochs=5000, batch_size=3000, verbose=0)
        else:
            model=0
        return model

     
    @classmethod
    def update_package_estimate(self):
        if self.sales_info_allowed and self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data_preseason(year=self.Year)
        if self.is_preseason_estimate and not self.sales_info_allowed:
            base_data, to_predict=SeasonTickets._query_no_sales_data_preseason(year=self.Year)
        if self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data(year=self.Year)
        if not self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_inseason_no_sales_data(year=self.Year)
        response=SeasonTickets._get_package_tickets(year=self.Year)
        if self.package_model_built:
            to_return=self.package_model.predict(to_predict)
            self.pack_prediction=to_return
            return
        else:
            model=SeasonTickets._build_package_model(self, train_data=base_data,
                                                      train_y=response)
            to_return=model.predict(to_predict)
            self.package_model=model
            self.pack_prediction=to_return
        return
        
    @classmethod
    def update_group_estimate(self):
        if self.sales_info_allowed and self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data_preseason(year=self.Year)
        if self.is_preseason_estimate and not self.sales_info_allowed:
            base_data, to_predict=SeasonTickets._query_no_sales_data_preseason(year=self.Year)
        if self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data(year=self.Year)
        if not self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_inseason_no_sales_data(year=self.Year)
        response=SeasonTickets._get_group_tickets(year=self.Year)
        if self.group_model_built:
            to_return=self.group_model.predict(to_predict)
            self.group_prediction=to_return
            return
        else:
            model=SeasonTickets._build_group_model(self, train_data=base_data,
                                                      train_y=response)
            to_return=model.predict(to_predict)
            self.group_model=model
            self.group_model_built=True
            self.group_prediction=to_return
        return
    
    @classmethod
    def update_inddisc_estimate(self):
        if self.sales_info_allowed and self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data_preseason(year=self.Year)
        if self.is_preseason_estimate and not self.sales_info_allowed:
            base_data, to_predict=SeasonTickets._query_no_sales_data_preseason(year=self.Year)
        if self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data(year=self.Year)
        if not self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_inseason_no_sales_data(year=self.Year)
        response=SeasonTickets._get_group_tickets(year=self.Year)
        if self.inddisc_model_built:
            to_return=self.inddisc_model.predict(to_predict)
            self.inddisc_prediction=to_return
            return
        else:
            model=SeasonTickets._build_inddisc_model(self, train_data=base_data,
                                                      train_y=response)
            to_return=model.predict(to_predict)
            self.inddisc_model=model
            self.inddisc_model_built=True
            self.inddisc_prediction=to_return
        return
        
    @classmethod
    def update_indfull_estimate(self):
        if self.sales_info_allowed and self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data_preseason(year=self.Year)
        if self.is_preseason_estimate and not self.sales_info_allowed:
            base_data, to_predict=SeasonTickets._query_no_sales_data_preseason(year=self.Year)
        if self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_all_data(year=self.Year)
        if not self.sales_info_allowed and not self.is_preseason_estimate:
            base_data, to_predict=SeasonTickets._query_inseason_no_sales_data(year=self.Year)
        response=SeasonTickets._get_group_tickets(year=self.Year)
        if self.indfull_model_built:
            to_return=self.indfull_model.predict(to_predict)
            self.indfull_prediction=to_return
            return
        else:
            model=SeasonTickets._build_indfull_model(self, train_data=base_data,
                                                      train_y=response)
            to_return=model.predict(to_predict)
            self.indfull_model=model
            self.indfull_model_built=True
            self.indfull_prediction=to_return
        return
    
        
        
class GameTickets:
    """
    Contains all game attributes: predictions at various levels, whether or not
    has been played, observations for the variables, etc...
    """
    
def fake_season(num_seasons, train_data, response):
    new_data=pd.DataFrame(columns=list(train_data))
    data_with_response=train_data.copy()
    data_with_response['Response']=response.copy()
    for k in range(num_seasons):
        sim_season=data_with_response.sample(n=81,replace=False)
        aggregated_season=sim_season.sum(axis=0)
        ag_season_df=aggregated_season.to_frame()
        ag_season_df=ag_season_df.transpose()
        new_data=new_data.append(ag_season_df)
    fake_train=new_data.copy()
    fake_y=fake_train['Response'].copy()
    del fake_train['Response']
    return fake_train, fake_y

def build_data():
    """Returns unscaled game-by-game ticketing data we have found to be relevant.
    Implemented on GameAttributes, hoping to change the structure of BF
    so that promos are easier to breakout and update, etc, by essentially copying
    the basic format of BrewHouse Games, Promos, etc table.
    """
    cnxn=pyodbc.connect('DRIVER={SQL Server};\
    SERVER=BREWBI-DB;DATABASE=BrewForecast;UID=PythonExecutor;PWD=2Cons!stant')
    cursor=cnxn.cursor()
    cols=list()
    base_data=pd.DataFrame()
    for row in cursor.columns(table='GameAttributes'):
        cols.append(row.column_name)
    try:
        cursor.execute("Select * from GameAttributes")
        for ix, row in enumerate(cursor.fetchall()):
            base_data=base_data.append(pd.DataFrame(pd.DataFrame(np.array(row)).transpose()))
        base_data=base_data.rename(columns=pd.Series(cols))
        base_data['Date']=pd.to_datetime(base_data['Date'], format='%Y-%m-%d').dt.date
        total_sold=pd.DataFrame()        
        cursor.execute("""select GameId, 
        PackageTickets, 
        GroupTickets, 
        INDDiscTickets, 
        INDFullTickets from BOByGame""" )
        for row in cursor.fetchall():
            total_sold=total_sold.append(pd.DataFrame(pd.DataFrame(np.array(row)).transpose()))
        total_sold=total_sold.rename(columns=pd.Series(['GameId',  'PackageTickets', 'GroupTickets', 'INDDiscTickets', 'INDFullTickets']))
        base_data=base_data.merge(total_sold, how='inner', on='GameId')
        to_delete=['MIL Opening Day Prospect Rankings', 'Mil Previous Season NLDS?', 
            'Opponent - Previous Season - GB Division', 'Opponent - Previous Season - GB Wild Card',
            'Opponent - Previous Season Playoffs?', 'Opponent - Previous Season Wild Card?',
            'Opponent - Previous Season NL/ALDS', 'Opponent - Previous Season NL/ALCS',
            'Opponent - Previous Season World Series', 'Good_game_indicator',
            'Great_game_indicator','SEA', 'TBR', 'Marquee', 'Regular', 'Bonus',
            'OpponentID', 'Other Theme Nights', 'ThemeNightID', 'SpecialDateID',
            'DiscountID1','DiscountID2','DiscountID3','DiscountID4', 'GiveawayID', 'GroupNightID1',
            'LaggedPerformance']
        for string in to_delete:
            del base_data[string]
        observed=base_data[base_data['Date']<datetime.date.today()]
        unobserved=base_data[base_data['Date']>=datetime.date.today()]
        #observed=base_data[base_data['Year']<2017]
        #unobserved=base_data[base_data['Year']>=2017]
    finally:
        cnxn.close()
    return {'observed':observed, 'unobserved':unobserved}
    
def write_baseline_predictions():
    """Writes our baseline predictions to BrewForecast
    """
    return True

def functional_model(*,input_dim, width, depth, skip_length, prob):
    """Builds deep NN model using Keras functional API.
    Which is a beautiful interface"""
    
    starting_point=0
    inputs=Input(shape=(input_dim,))
    x=normalization.BatchNormalization()(inputs)
    x=Dense(width, activation='relu')(inputs)
    x=normalization.BatchNormalization()(x)
    for k in range(depth):
        if k%skip_length==0:
            if starting_point!=0:
                x=Add()([x, starting_point])
            starting_point=x
        x=Dense(width, activation='relu')(x)
        x=Dropout(prob)(x)
        x=normalization.BatchNormalization()(x)
    prediction=Dense(1, activation='linear')(x)
    model=Model(inputs=inputs, outputs=prediction)
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model
    
def GBG_modeler_and_predictor(*,name, train_data, to_predict, num_iter=15, num_to_throw_out=5):
    """Hardcoded parameters for these models, built essentially with cross-
    validation
    """
    train_x, train_y=train_data
    To_predict_dates=pd.DataFrame(to_predict['Date'], columns=['Date'])
    To_predict_dates.reset_index(drop=True, inplace=True)
    to_delete=['Date']
    for string in to_delete:
        del train_x[string]
        del to_predict[string]
    scaler=StandardScaler().fit(train_x)
    train_x=pd.DataFrame(scaler.transform(train_x), columns=list(train_x))
    to_predict=pd.DataFrame(scaler.transform(to_predict), columns=list(to_predict))
    w=120
    d=12
    p=.18
    skips=3
    pat=np.random.randint(low=100, high=120)
    early_stopping=EarlyStopping(monitor='val_loss', min_delta=1, patience=pat)
    model=functional_model(input_dim=len(train_x.columns),width=w, depth=d, skip_length=skips,prob=p)
    running_predictions=pd.DataFrame([0]*len(to_predict))
    for k in range(num_iter):
        msk=np.random.rand(len(train_x))<0.8
        val_x=train_x[~msk]
        val_y=train_y[~msk]
        split_x=train_x[msk]
        split_y=train_y[msk]
        model.fit(np.asmatrix(split_x), np.asmatrix(split_y).transpose(),
                  validation_data=(np.asmatrix(val_x), np.asmatrix(val_y).transpose()),
                    callbacks=[early_stopping], epochs=5000, batch_size=100, verbose=0)
        if k>=num_to_throw_out:
            running_predictions=running_predictions+pd.DataFrame(model.predict(np.asmatrix(to_predict)))
    To_predict_dates[name]=running_predictions/(k-num_to_throw_out+1)
    print(To_predict_dates)
    return To_predict_dates
    
def Full_season_modeler_and_predictor(*,name, train_data, to_predict, num_seasons=500):
    """
    Function to build probability distributions for seasonlong numbers, based on
    sampling observed games. Again basically built with CV
    """
    train_x, train_y=train_data
    del train_x['Date']
    del to_predict['Date']
    fake_x, fake_y=fake_season(num_seasons, train_x.copy(), train_y.copy())
    aggregated_to_predict=pd.DataFrame(to_predict.sum(axis=0)).transpose()
    aggregated_to_predict=aggregated_to_predict[list(fake_x)]
    scaler=StandardScaler().fit(fake_x)
    scaled_x=pd.DataFrame(scaler.transform(fake_x), columns=list(fake_x))
    scaled_to_predict=pd.DataFrame(scaler.transform(aggregated_to_predict),
                                   columns=list(aggregated_to_predict))
    if name=='PackageTickets':
        model=linear_model.LassoLarsIC(criterion='bic')
        model.fit(scaled_x, fake_y)
        prediction=model.predict(scaled_to_predict)
    elif name=='GroupTickets':
        ab_clf=GridSearchCV(ensemble.AdaBoostRegressor(), ab_parameters, cv=10)
        ab_clf.fit(scaled_x, fake_y)
        prediction=ab_clf.predict(scaled_to_predict)
    elif name=='INDFullTickets':
        IF_lasso=linear_model.LassoLarsIC(criterion='bic')
        IF_lasso.fit(scaled_x, fake_y)
        prediction=IF_lasso.predict(scaled_to_predict)
    elif name=='INDDiscTickets':
        ID_lasso=linear_model.LassoLarsIC(criterion='bic')
        ID_lasso.fit(scaled_x, fake_y)
        prediction=ID_lasso.predict(scaled_to_predict)
    elif name=='AllND':
        w=120
        d=12
        p=.18
        skips=3
        pat=np.random.randint(low=100, high=120)
        early_stopping=EarlyStopping(monitor='val_loss', min_delta=1, patience=pat)
        model=functional_model(input_dim=len(train_x.columns),width=w, depth=d, skip_length=skips,prob=p)
        msk=np.random.rand(len(scaled_x))<0.8
        val_x=scaled_x[~msk]
        val_y=fake_y[~msk]
        split_x=scaled_x[msk]
        split_y=fake_y[msk]
        model.fit(np.asmatrix(split_x), np.asmatrix(split_y).transpose(),
                  validation_data=(np.asmatrix(val_x), np.asmatrix(val_y).transpose()),
                    callbacks=[early_stopping], epochs=5000, batch_size=3000, verbose=0)
        prediction=model.predict(np.asmatrix(scaled_to_predict))[0]
    else:
        prediction="You Fucked Up!"
    return prediction

###START OF MAIN SCRIPT##################

  
built_data=build_data()
observed_data=built_data['observed']
###I don't want to commit anything to BF until I have a decent actual estimate
##of the 2018 schedule; committing this now will just encourage laziness, lack of 
##checking the variables in the future--treating it like a black box. I don't want
##that so for now I read locally.
to_predict_data=pd.read_csv("~/Documents/2018 schedule/2018_encoded_promos_smashed_together.csv")

To_delete_2018=['Time', 'Opp', 'MIL Opening Day Prospect Rankings', 'Mil Previous Season NLDS?', 
        'Opponent - Previous Season - GB Division', 'Opponent - Previous Season - GB Wild Card',
        'Opponent - Previous Season Playoffs?', 'Opponent - Previous Season Wild Card?',
        'Opponent - Previous Season NL/ALDS','Opponent - Previous Season NL/ALCS',
        'Opponent - Previous Season World Series', 'Good_game_indicator',
        'Great_game_indicator','SEA', 'TBR', 'Marquee', 'Regular', 'Bonus', 'OpponentID',
         'Other Theme Nights', 'LaggedPerformance']
    
To_vary_2018=['Previous Season Wins', 'Previous Season TV Ratings', 'MIL - Previous Season Playoffs?',
              'Mil Previous Season NLDS?', 'MIL Previous Season NLCS?', 'Mil Off Season Spend',
              'Standings', 'Win %', 'LaggedPerformance']
for string in To_delete_2018:
    del to_predict_data[string]

response_dict={}
GBGPredictions=pd.DataFrame({'Date':to_predict_data['Date']})
for string in ['PackageTickets', 'GroupTickets', 'INDFullTickets', 
'INDDiscTickets']:
    response_dict[string]=observed_data[string]
    del observed_data[string]
    del to_predict_data[string]
del observed_data['GameId']
to_predict_data=to_predict_data[list(observed_data)]
response_dict['AllND']=response_dict['PackageTickets']+response_dict['GroupTickets']\
+response_dict['INDFullTickets']+response_dict['INDDiscTickets']
FullSeasonPredictions=pd.DataFrame(columns=['AllND','PackageTickets', 'GroupTickets',
                                            'INDFullTickets', 'INDDiscTickets'])
#for k in range(100):
#    to_predict_data['Win %']=np.random.uniform(low=0.4, high=0.5)
#    to_predict_data['Standings']=np.random.uniform(low=-10, high=0)
#    to_predict_data['Previous Season Wins']=np.random.uniform(low=78, high=85)
#    to_predict_data['Previous Season TV Ratings']=np.random.uniform(low=3.5, high=4)
#    to_predict_data['MIL - Previous Season Playoffs']=0#np.random.uniform(low=0, high=.2)
#    to_predict_data['Mil Previous Season NLDS?']=0#np.random.uniform(low=0, high=.05)
#    to_predict_data['MIL Previos Season NLCS?']=0#np.random.uniform(low=0, high=.01)
#    to_predict_data['Mil Off Season Spend']=0#np.random.uniform(low=0, high=5)
#    
#
#
#    ticket_dict={}
#    for string in ['PackageTickets', 'GroupTickets', 'INDFullTickets', 
#    'INDDiscTickets']:
#        FSPreds=Full_season_modeler_and_predictor(name=string, train_data=(observed_data.copy(),
#                                                              response_dict[string].copy()),
#                to_predict=to_predict_data.copy(), num_seasons=10000)
#        print(string+" prediction is "+str(FSPreds))
#        ticket_dict[string]=FSPreds
#    FullSeasonPredictions=pd.concat([FullSeasonPredictions, 
#                                     pd.DataFrame(ticket_dict)])
    
for k in range(10):
    to_predict_data['Win %']=np.random.uniform(low=0.4, high=0.5)
    to_predict_data['Standings']=np.random.uniform(low=-10, high=0)
    to_predict_data['Previous Season Wins']=np.random.uniform(low=78, high=85)
    to_predict_data['Previous Season TV Ratings']=np.random.uniform(low=3.5, high=4)
    to_predict_data['MIL - Previous Season Playoffs?']=0#np.random.uniform(low=0, high=.2)
    #to_predict_data['Mil Previous Season NLDS?']=0#np.random.uniform(low=0, high=.05)
    to_predict_data['MIL Previous Season NLCS?']=0#np.random.uniform(low=0, high=.01)
    to_predict_data['Mil Off Season Spend (Millions)']=0#np.random.uniform(low=0, high=5)
    for string in ['PackageTickets', 'GroupTickets', 'INDFullTickets', 
    'INDDiscTickets']:
        preds=GBG_modeler_and_predictor(name=string,
                    train_data=(observed_data.copy(), response_dict[string].copy()),
                                to_predict=to_predict_data.copy(), num_iter=20,
                                num_to_throw_out=10)
        GBGPredictions=pd.merge(GBGPredictions, pd.DataFrame(preds), 
                             how='inner',on='Date')